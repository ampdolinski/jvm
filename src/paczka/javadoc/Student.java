package paczka.javadoc;

import java.util.Set;

public class Student {

    /**
     * Declaration of parameters of Student class
     */
    private final String firstName;
    private final String lastName;
    private final int age;
    private final Set<Course> courses;


    /**
     * Student constructor, creaetes a student with description given in parameter.
     * @param firstName - first name of student
     * @param lastName - last name of student
     * @param age - age of student
     * @param courses - courses of student
     */
    public Student(String firstName, String lastName, int age, Set<Course> courses) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.courses = courses;
    }

    /**
     * Returning first name of student
     * @return - first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Returning last name of student
     * @return - last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Returning age of student
     * @return - age
     */
    public int getAge() {
        return age;
    }

    /**
     * Returning courses of student
     * @return - courses
     */
    public Set<Course> getCourses() {
        return courses;
    }
}