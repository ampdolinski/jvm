package paczka.javadoc;

import java.util.List;
import java.util.Set;

/**
 * Class implments interface of School
 */
public class SDAcademySchool implements School {

    /**
     * Adding a new student to the school
     * @param student - new student added to school
     */
    @Override
    public void addStudent(Student student) {}

    /**
     * Creates and adds a new student to the school; method requires all parameters to create a single student
     * @param firstName - first name of a new student
     * @param lastName - last name of a new student
     * @param age - age of a new student
     * @param courses - courses that a new student attends
     */
    @Override
    public void addStudent(String firstName, String lastName, int age, Set<Course> courses) {}

    /**
     *Searching a student with its first and last name
     * @param firstName - first name of student
     * @param lastName - last name of student
     * @return - checking is that person is a student or not; if is, then shows its parameters
     */
    @Override
    public Student getStudentByFirstNameAndLastName(String firstName, String lastName) {
        return null;
    }

    /**
     * Creates a list of all students attending to selected course
     * @param course - what course would be checked for list of students
     * @return - a list of all students attending to selected course
     */
    @Override
    public List<Student> getStudentsByCourse(Course course) {
        return null;
    }
}