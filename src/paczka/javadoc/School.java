package paczka.javadoc;

import java.util.List;
import java.util.Set;

/**
 * interface of School;
 *
 */
public interface School {
    /**
     * prepared method to add a new student,
     * based on Student class
     * @param student -
     */
    void addStudent(Student student);
    void addStudent(String firstName, String lastName, int age, Set<Course> courses);
    Student getStudentByFirstNameAndLastName(String firstName, String lastName);
    List<Student> getStudentsByCourse(Course course);
}